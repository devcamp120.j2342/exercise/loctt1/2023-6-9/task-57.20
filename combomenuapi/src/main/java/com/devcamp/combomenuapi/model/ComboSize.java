package com.devcamp.combomenuapi.model;

public class ComboSize {
    private String duongKinh;
    private int suonNuong;
    private String salad;
    private int nuocNgot;
    private double gia;

    public String getDuongKinh() {
        return duongKinh;
    }

    public void setDuongKinh(String duongKinh) {
        this.duongKinh = duongKinh;
    }

    public int getSuonNuong() {
        return suonNuong;
    }

    public void setSuonNuong(int suonNuong) {
        this.suonNuong = suonNuong;
    }

    public String getSalad() {
        return salad;
    }

    public void setSalad(String salad) {
        this.salad = salad;
    }

    public int getNuocNgot() {
        return nuocNgot;
    }

    public void setNuocNgot(int nuocNgot) {
        this.nuocNgot = nuocNgot;
    }

    public double getGia() {
        return gia;
    }

    public void setGia(double gia) {
        this.gia = gia;
    }

    public ComboSize(String duongKinh, int suonNuong, String salad, int nuocNgot, double gia) {
        this.duongKinh = duongKinh;
        this.suonNuong = suonNuong;
        this.salad = salad;
        this.nuocNgot = nuocNgot;

        this.gia = gia;
    }

    public ComboSize() {
    }

    public ComboSize(String duongKinh, int suonNuong, String salad, int nuocNgot) {
        this.duongKinh = duongKinh;
        this.suonNuong = suonNuong;
        this.salad = salad;
        this.nuocNgot = nuocNgot;
    }

}
