package com.devcamp.combomenuapi.model;

public class Menu {
    private String size;
    private ComboSize comboSize;

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public ComboSize getComboSize() {
        return comboSize;
    }

    public void setComboSize(ComboSize comboSize) {
        this.comboSize = comboSize;
    }

    public Menu(String size, ComboSize comboSize) {
        this.size = size;
        this.comboSize = comboSize;
    }

    public Menu() {
    }

    public Menu(String size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "Menu [size=" + size + ", comboSize=" + comboSize + "]";
    }

}