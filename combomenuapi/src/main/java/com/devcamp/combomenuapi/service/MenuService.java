package com.devcamp.combomenuapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.combomenuapi.model.Menu;

@Service
public class MenuService {
    @Autowired
    private ComboSizeService comboSizeService;
    Menu menuS = new Menu("S (small)");
    Menu menuM = new Menu("M (medium)");
    Menu menuL = new Menu("L (large)");

    public ArrayList<Menu> allMenus() {
        menuS.setComboSize(comboSizeService.comboSizeS);
        menuM.setComboSize(comboSizeService.comboSizeM);
        menuL.setComboSize(comboSizeService.comboSizeL);
        ArrayList<Menu> menus = new ArrayList<>();
        menus.add(menuM);
        menus.add(menuL);
        menus.add(menuS);

        return menus;

    }
}
