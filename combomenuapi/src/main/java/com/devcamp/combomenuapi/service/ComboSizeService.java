package com.devcamp.combomenuapi.service;

import org.springframework.stereotype.Service;

import com.devcamp.combomenuapi.model.ComboSize;

@Service
public class ComboSizeService {
    ComboSize comboSizeS = new ComboSize("20 cm", 2, "200 g", 2, 150);
    ComboSize comboSizeM = new ComboSize("25 cm", 4, "300 g", 3, 200);
    ComboSize comboSizeL = new ComboSize("30 cm", 8, "500 g", 4, 250);

}
