package com.devcamp.combomenuapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.combomenuapi.model.Menu;
import com.devcamp.combomenuapi.service.MenuService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class MenuController {
    @Autowired
    private MenuService menuService;

    @GetMapping("/menus")
    public ArrayList<Menu> menulist() {
        ArrayList<Menu> menus = menuService.allMenus();

        return menus;
    }

}
